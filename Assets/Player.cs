﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.Characters.FirstPerson;

public static class Player {

	public static float life = 100;
	public static bool isDead = false;



	public static void DoDamage(){
		life -= (8 + Mathf.Floor (Random.Range (0, 4)));
		if(life <= 0){
			life = 0;
			KillPlayer();
		}
	}


	static void KillPlayer(){
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		player.GetComponent<FirstPersonController> ().enable_move(false); 
		isDead = true;
		game_manager.state = game_state.GAME_OVER;
	}
}
