﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemy_move : MonoBehaviour {

	UnityEngine.AI.NavMeshAgent navMeshAgent;
	Transform playerTransform;

	void Start () {
		navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
		playerTransform = GameObject.FindWithTag("Player").transform;
	}

	void Update () {
		navMeshAgent.SetDestination(playerTransform.position);
	}
}
