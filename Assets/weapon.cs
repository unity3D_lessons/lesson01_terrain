﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weapon : MonoBehaviour {
	public GameObject bullet;
	public GameObject gun;
	private int no_bullet_out = 0;

	public int get_bullet()
	{
		return no_bullet_out;
	}
	public void decrease_no_bullet()
	{
		if (no_bullet_out != 0) {
			no_bullet_out--;
		}
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (no_bullet_out < 10 && (game_manager.state == game_state.GAME_RUN)) {
			if (Input.GetMouseButtonDown (0)) {
				GameObject copy_bullet = 
					Instantiate<GameObject> (bullet);

				copy_bullet.transform.position = gun.transform.position;

				copy_bullet.GetComponent<Rigidbody> ().AddForce (gun.transform.forward * 500);
				no_bullet_out++;
			}
		}
	}
}
