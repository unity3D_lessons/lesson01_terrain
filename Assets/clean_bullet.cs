﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clean_bullet : MonoBehaviour {
	GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		Destroy (gameObject, 5);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnDestroy() {
		weapon player_weapon = player.GetComponentInChildren<weapon> ();
		player_weapon.decrease_no_bullet ();
		int no_bullet = player_weapon.get_bullet();
		print (no_bullet);
	}
		
}

