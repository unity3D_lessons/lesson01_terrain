﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class exit_action : MonoBehaviour {
	public Canvas exit_menu;
	public static bool is_visible = false;

	private game_state last_state;
	// Use this for initialization
	void Start () {
		Cursor.visible = is_visible;
		print ("Kursor tu znika");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			exit_menu.enabled = !exit_menu.enabled;
			is_visible = exit_menu.enabled;

			Cursor.visible = is_visible;
			if (exit_menu.enabled == false) {
				Time.timeScale = 1.0f;
				game_manager.state = last_state;
			}else {
				Time.timeScale = 0.0f;
				last_state = game_manager.state; 
				game_manager.state = game_state.GAME_MENU;
			}
		}	
	}
	public void on_exit() {
		print ("exit");
		Application.Quit ();
	}
}
