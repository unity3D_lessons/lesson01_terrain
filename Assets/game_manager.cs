﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum game_state
{
	GAME_RUN = 0,
	GAME_PAUSE,
	GAME_MENU,
	GAME_OVER
};

public static class game_manager {
	public static game_state state = game_state.GAME_RUN;
}
