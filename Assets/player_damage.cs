﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_damage : MonoBehaviour {

	public float rate_hit_time= 5f;
	float next_hit_time = 0f;

	// Use this for initialization
	void Start () {
		
	}

	void OnTriggerStay(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (Time.time >= next_hit_time) {
				Player.DoDamage ();
				next_hit_time = Time.time + rate_hit_time;
				print ("Life remain: " + Player.life);
			}
		}
	}	
	// Update is called once per frame
	void Update () {
		
	}
}
