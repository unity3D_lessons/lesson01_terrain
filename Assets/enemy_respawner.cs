﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_respawner : MonoBehaviour {

	public float rateTime = 5f; //co jaki czas ma się wykonywać
	float nextSpawnTime = 0f; //kiedy następne wykonanie
	GameObject[] respawns;
	public GameObject enemy1;
	public GameObject enemy2;
	GameObject selectedEnemy;

	void Start(){
		respawns = GameObject.FindGameObjectsWithTag("Respawn");
		print (respawns.Length);
	}

	void Update(){
		if(nextSpawnTime <= Time.time){
			SpawnEnemy(); //coś co ma się wykonać co określony czas
			nextSpawnTime += rateTime;
		}
	}

	void SpawnEnemy(){
		int index = Random.Range (0, respawns.Length);
		int enemy_idx = Random.Range (0, 2);
		switch (enemy_idx) { 
		case 0: 
			selectedEnemy = enemy1;
			break;
		case 1:
			selectedEnemy = enemy2;
			break;
		default :
			selectedEnemy = enemy1;
			break;
		}
		GameObject new_enemy = Instantiate<GameObject> (selectedEnemy);
		new_enemy.transform.position = respawns [index].transform.position;
	}
}
